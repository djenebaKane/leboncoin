//
//  Category.swift
//  leboncoin
//
//  Created by Mac2 on 20/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation



struct Category : Decodable {
    
    let id : Int
    let name : String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    public init(id: Int, name: String){
        self.id = id
        self.name = name
    }
    
    init(from decoder: Decoder) throws {
        let modelContainer = try decoder.container(keyedBy: CodingKeys.self)
        id = try modelContainer.decode(Int.self, forKey: .id)
        name = try modelContainer.decode(String.self, forKey: .name)
    }
}
