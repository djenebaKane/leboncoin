//
//  Item.swift
//  leboncoin
//
//  Created by Mac2 on 20/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation



public struct Item : Codable {
    let id : Int
    let category_id : Int
    let title : String
    let description : String
    let price : Float
    let images_url : ImageSize
    let creation_date : String
    let is_urgent : Bool
    let siret : String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case category_id
        case title
        case description
        case price
        case images_url
        case creation_date
        case is_urgent
        case siret
    }
    
    public init(id: Int, category_id: Int, title: String, description: String, price: Float, images_url: ImageSize, creation_date: String, is_urgent: Bool, siret: String?){
        self.id = id
        self.category_id = category_id
        self.title = title
        self.description = description
        self.price = price
        self.images_url = images_url
        self.creation_date = creation_date
        self.is_urgent = is_urgent
        self.siret = siret
    }
    
    public init(from decoder: Decoder) throws {
        let modelContainer = try decoder.container(keyedBy: CodingKeys.self)
        id = try modelContainer.decode(Int.self, forKey: .id)
        category_id = try modelContainer.decode(Int.self, forKey: .category_id)
        title = try modelContainer.decode(String.self, forKey: .title)
        description = try modelContainer.decode(String.self, forKey: .description)
        price = try modelContainer.decode(Float.self, forKey: .price)
        images_url = try modelContainer.decode(ImageSize.self, forKey: .images_url)
        creation_date = try modelContainer.decode(String.self, forKey: .creation_date)
        is_urgent = try modelContainer.decode(Bool.self, forKey: .is_urgent)
        siret = try modelContainer.decodeIfPresent(String.self, forKey: .siret)
        
    }
    
    

}

public struct ImageSize : Codable {
    let small : String?
    let thumb : String?
    
    
    enum CodingKeys: String, CodingKey {
        case small
        case thumb
    }
    
    public init(small: String, thumb: String) {
        self.small = small
        self.thumb = thumb
    }
    public init(from decoder: Decoder) throws {
        let modelContainer = try decoder.container(keyedBy: CodingKeys.self)
        small = try modelContainer.decodeIfPresent(String.self, forKey: .small)
        thumb = try modelContainer.decodeIfPresent(String.self, forKey: .thumb)
       
    }
}
extension Item {
    func getCategoryName(catList: [Category]) -> String? {
        return catList.first(where: {
            $0.id == self.category_id
            })?.name
    }
}

// MARK: Array extension
extension Array where Element == Item {
    
    func matching(_ query: Int) -> [Item] {
        return self.filter({ $0.category_id == query })
    }

    func sortItems() -> [Item] {
        return self.sorted(by: { first, next in
            if first.is_urgent != next.is_urgent { // first sort by is_urgent
                return first.is_urgent && !next.is_urgent
            } else {//then createDate orderedDescending ("First Date is greater then second date"))
                return (first.creation_date.toDate() ?? Date()).compare(next.creation_date.toDate() ?? Date()) == .orderedDescending
            }
        })
    }
}

    

