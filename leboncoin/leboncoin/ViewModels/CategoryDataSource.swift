//
//  CategoryDataSource.swift
//  leboncoin
//
//  Created by Mac2 on 23/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import UIKit
class CategoryDataSource : GenericDataSource<Category>, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) 
        let category = self.data.value[indexPath.row]
        cell.textLabel?.textColor = .textAccentColor
        cell.textLabel?.text = category.name
        return cell
    }

}
