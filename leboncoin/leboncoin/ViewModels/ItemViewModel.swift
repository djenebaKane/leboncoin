//
//  ItemViewModel.swift
//  leboncoin
//
//  Created by Mac2 on 22/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation

struct ItemViewModel {
    // MARK: - Properties
    weak var dataSource : GenericDataSource<Item>?
    
    var delegate: ErrorDelegate!
    
    // MARK: - Init
    init(dataSource: GenericDataSource<Item>?, delegate: ErrorDelegate) {
        self.dataSource = dataSource
        self.delegate = delegate
    }
    
    // MARK: - Functions
    func fetchItems(categoryId : Int? = nil) {
        APIClient.shared.getItemList(completion: { (response) in
            switch response {
            case .success(data: let itemList):
                print("itemList", itemList)
                // reload data
                if let catId = categoryId {
                    self.dataSource?.data.value = itemList.matching(catId).sortItems()
                } else {
                    self.dataSource?.data.value = itemList.sortItems()
                }
                
                break
            case .failure(error: let error):
                print("error", error.localizedDescription)
                self.delegate.onGetError(error: error)
                break
            }
            
        })
    }

}
protocol ErrorDelegate {
    func onGetError(error : Error)
}
