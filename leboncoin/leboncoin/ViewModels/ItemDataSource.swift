//
//  ItemDataSource.swift
//  leboncoin
//
//  Created by Mac2 on 22/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import UIKit
class ItemDataSource : GenericDataSource<Item>, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0;//Choose your custom row height
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
        let item = self.data.value[indexPath.row]
        cell.item = item
        return cell
    }

}
