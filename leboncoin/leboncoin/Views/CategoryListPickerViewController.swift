//
//  CategoryListPickerViewController.swift
//  leboncoin
//
//  Created by Mac2 on 23/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import UIKit

class CategoryListPickerViewController: UIViewController, UITableViewDelegate {
    
    // MARK: - Properties
    let tableView = UITableView()
    var safeArea: UILayoutGuide!
    let categoryDataSource = CategoryDataSource()
    var delegate : CategoryPickerDelegate?
    
    // MARK: - Override Functions
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        categoryDataSource.data.value = ItemListViewController.categoryList
        configureTableView()
        safeArea = view.layoutMarginsGuide
        setupViews()
        self.tableView.reloadData()
    }

    // MARK: - Configure Views Functions
    func configureTableView(){
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.dataSource = self.categoryDataSource
        self.tableView.delegate = self
    }
    
    func setupViews() {
        view.addSubview(tableView)
        tableView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 10, paddingRight: 0)
    }
    
    // MARK: - TableView
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor.accentLightColor
        delegate?.onSelectedCategory(selectedCat: self.categoryDataSource.data.value[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}

protocol  CategoryPickerDelegate {
    func onSelectedCategory(selectedCat : Category)
}
