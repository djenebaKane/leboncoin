//
//  ItemListViewController.swift
//  leboncoin
//
//  Created by Mac2 on 22/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import UIKit

class ItemListViewController: UIViewController, UITableViewDelegate {
    
    // MARK: - Properties
    let tableView = UITableView()
    var safeArea: UILayoutGuide!
    
    let itemDataSource = ItemDataSource()

    static var categoryList = [Category]()
    
    var dispatchGroup = DispatchGroup()
    
    var dispatchGroupCounter = 0
    
    lazy var viewModel : ItemViewModel = {
        let viewModel = ItemViewModel(dataSource: itemDataSource, delegate: self)
        return viewModel
    }()
    
    // MARK: - Outlets Properties
    private let progressView : UIActivityIndicatorView = {
        let prgView = UIActivityIndicatorView(style: .whiteLarge)
        prgView.hidesWhenStopped = true
        prgView.color = UIColor.accentColor
        return prgView
    }()
    
    private let filterTextField : UITextField = {
        let txtview = UITextField()
        txtview.textColor = .textAccentColor
        txtview.font = UIFont.boldSystemFont(ofSize: 16)
        txtview.textAlignment = .center
        txtview.borderColor = .textAccentColor
        txtview.borderWidth = 0.5
        txtview.cornerRadius = 5
        txtview.placeholder = "Selectionner une catégorie"
        
        return txtview
    }()

    // MARK: - Override Functions
    override func loadView() {
        super.loadView()
        
        configureTableView()
        setupViews()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onFilterClick(tapGestureRecognizer:)))
        // - Disable Filter before WS result
        filterTextField.isEnabled = false
        filterTextField.addGestureRecognizer(gesture)
        
        progressView.startAnimating()
        
        notifiCallbacks()
        viewModel.fetchItems()
        fetchCategories()
        
        dispatchGroup.enter()
        dispatchGroup.enter()
        dispatchGroupCounter = 2
    }
    
    func configureTableView(){
        safeArea = view.layoutMarginsGuide
        tableView.register(ItemCell.self, forCellReuseIdentifier: ItemCell.cellId)
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        self.tableView.dataSource = self.itemDataSource
        self.tableView.delegate = self
    }
    
    func setupViews() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        view.addSubview(filterTextField)
        
        tableView.addSubview(progressView)
        
        filterTextField.anchor(top: safeArea.topAnchor, left: view.leftAnchor, bottom: tableView.topAnchor, right: view.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)
        
        tableView.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 10, paddingRight: 10)
        
        progressView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }

    // MARK: - TableView
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor.accentLightColor
        let itemDetailVC = ItemDetailsViewController()
        itemDetailVC.item = self.itemDataSource.data.value[indexPath.row]
        self.navigationController?.pushViewController(itemDetailVC, animated: true)
    }
    
    // MARK: - Others Functions
    @objc func onFilterClick(tapGestureRecognizer: UITapGestureRecognizer? = nil){
        let vc = CategoryListPickerViewController()
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func notifiCallbacks(){
        self.itemDataSource.data.addAndNotify(observer: self) { [weak self] in
            if ((self?.dispatchGroupCounter ?? 0) > 0) {
                self?.dispatchGroup.leave()
                self?.dispatchGroupCounter = (self?.dispatchGroupCounter ?? 0) - 1
                if (self?.dispatchGroupCounter == 0) {
                    self?.tableView.reloadData()
                    self?.filterTextField.isEnabled = true
                    self?.progressView.stopAnimating()
                }
            }
        }
    }
    
    func fetchCategories(){
        APIClient.shared.getCategoryList(completion: { (response) in
            switch response {
            case .success(data: let categoryList):
                self.dispatchGroup.leave()
                self.dispatchGroupCounter = self.dispatchGroupCounter - 1
                if (self.dispatchGroupCounter  <= 0) { // last one to finish
                    self.tableView.reloadData()
                    self.progressView.stopAnimating()
                    self.filterTextField.isEnabled = true
                }
                // reload data
                ItemListViewController.categoryList = categoryList
                break
            case .failure(error: let error):
                self.dispatchGroup.leave()
                self.dispatchGroupCounter = self.dispatchGroupCounter - 1
                ItemListViewController.categoryList = [Category]()
                if (self.dispatchGroupCounter  <= 0) { // last one to finish
                    self.filterTextField.isEnabled = true
                    self.progressView.stopAnimating()
                    self.tableView.reloadData()
                }
                self.displayAlert(title: "Erreur Recupération Catégories", msg: error.localizedDescription, style: .alert)
                break
            }
        })
    }
}

extension ItemListViewController : CategoryPickerDelegate, ErrorDelegate {
    func onGetError(error: Error) {
        self.progressView.stopAnimating()
        self.displayAlert(title: "Erreur Recupération des items", msg: error.localizedDescription, style: .alert)
    }
    
    func onSelectedCategory(selectedCat: Category) {
        self.filterTextField.text = selectedCat.name
        self.dispatchGroup.enter()
        self.dispatchGroupCounter = self.dispatchGroupCounter + 1
        self.viewModel.fetchItems(categoryId : selectedCat.id)
        self.tableView.reloadData()
    }
  
}
