//
//  ItemDetailsViewController.swift
//  leboncoin
//
//  Created by Mac2 on 23/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import UIKit

class ItemDetailsViewController: UIViewController, UIScrollViewDelegate {
    // MARK: - Properties
    var item : Item?
    var safeArea: UILayoutGuide!
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    // MARK: - Outlet Properties
    private let progressView : UIActivityIndicatorView = {
        let prgView = UIActivityIndicatorView(style: .gray)
        prgView.hidesWhenStopped = true
        prgView.color = UIColor.accentColor
        return prgView
    }()
    private let itemImage : UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
    return imgView
    }()
    
    private let itemTitleLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textAccentColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        lbl.setContentHuggingPriority(UILayoutPriority(rawValue: 749), for: .vertical)
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let itemPriceLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textAccentColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.setContentHuggingPriority(UILayoutPriority(rawValue: 749), for: .vertical)
        lbl.textAlignment = .left
        return lbl
    }()
    
    private let itemCategoryLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textColor
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.setContentHuggingPriority(UILayoutPriority(rawValue: 749), for: .vertical)
        lbl.textAlignment = .left
        return lbl
    }()
    

    private let itemUrgentLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .accentColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .right
        return lbl
    }()
    
    private let itemDateLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        return lbl
    }()
    
    
    private let itemSiretLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .right
        return lbl
    }()
    
    private let itemIdLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .right
        return lbl
    }()
    
    private let itemDescriptionLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textColor
        lbl.font = UIFont.italicSystemFont(ofSize: 14)
        lbl.textAlignment = .justified
        lbl.numberOfLines = 0
        return lbl
    }()
    // MARK: - Override Functions
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        setupScrollView()
        setupViews()
        fillContent()
    }
    
    // MARK: - Configure Views Functions
    func setupScrollView(){
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
    }

    func setupViews() {
        
        // - add subviews
        contentView.addSubview(itemImage)
        
        contentView.addSubview(itemTitleLabel)
        
        contentView.addSubview(itemPriceLabel)
        contentView.addSubview(itemCategoryLabel)
        
        contentView.addSubview(itemUrgentLabel)
        contentView.addSubview(itemDateLabel)
        
        contentView.addSubview(itemSiretLabel)
        contentView.addSubview(itemIdLabel)
    
        contentView.addSubview(itemDescriptionLabel)
        contentView.addSubview(progressView)

        // - Set up Constraint
        let stackView = UIStackView(arrangedSubviews: [itemDateLabel,itemSiretLabel,itemUrgentLabel])
        stackView.distribution = .equalSpacing
        stackView.axis = .horizontal
        stackView.spacing = 5
        stackView.alignment = .center
        contentView.addSubview(stackView)
        
        
        itemImage.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: itemTitleLabel.topAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)
        
        itemTitleLabel.anchor(top: nil, left: contentView.leftAnchor, bottom: itemPriceLabel.topAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)

        itemPriceLabel.anchor(top: nil, left: contentView.leftAnchor, bottom: stackView.topAnchor, right: itemCategoryLabel.leftAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)

        itemCategoryLabel.anchor(top: nil, left: nil, bottom: stackView.topAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)
        
        stackView.anchor(top: nil, left: contentView.leftAnchor, bottom: itemDescriptionLabel.topAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)
        
        itemDescriptionLabel.anchor(top: nil, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)
        
        progressView.anchor(top: nil, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
    }
    
    // MARK: - Others Functions
    func fillContent() {
        // - Set up content
        if let item = item {
            progressView.startAnimating()
            self.itemImage.image = nil
            ImageUtils.downloadImageAsynchronous(urlString: item.images_url.thumb){ image  in
                self.progressView.stopAnimating()
                if let img = image {
                    self.itemImage.image = img
                }
            }
            itemTitleLabel.text = item.title
            itemPriceLabel.text = "\(item.price)" + " €"
            
            itemCategoryLabel.text = item.getCategoryName(catList: ItemListViewController.categoryList)
            
            itemUrgentLabel.text = "Urgent"
            itemUrgentLabel.isHidden = !item.is_urgent
            
            itemDateLabel.text = item.creation_date.dateUTCToLocal()
            
            if let siret = item.siret {
                itemSiretLabel.text = "Siret :" + siret
            } else {
                itemSiretLabel.isHidden = true
            }
            
            itemIdLabel.text = "\(item.id)"
            
            itemDescriptionLabel.text = item.description

        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
