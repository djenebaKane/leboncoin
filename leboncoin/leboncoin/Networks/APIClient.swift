//
//  APIClient.swift
//  leboncoin
//
//  Created by Mac2 on 21/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation


class APIClient : Client {
    
    static let shared = APIClient()
    
    func getItemList(completion:@escaping (Result<[Item]>)->Void) {
        performRequest(route: APIRouter.getItemList, completion: completion)
    }
    
    func getCategoryList(completion:@escaping (Result<[Category]>)->Void) {
        performRequest(route: APIRouter.getCategoryList, completion: completion)
    }
}
