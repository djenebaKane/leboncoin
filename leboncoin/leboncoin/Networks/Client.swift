//
//  Client.swift
//  leboncoin
//
//  Created by Mac2 on 21/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation

class Client {
    
    private let config: URLSessionConfiguration
    private let session: URLSession
    
    public init() {
        config = URLSessionConfiguration.default
        session = URLSession(configuration: config)
    }
    
    enum Result<T> {
        case success(data: T)
        case failure(error: Error)
    }
    
    // MARK: - Request Web Service 
    func performRequest<T:Decodable>(route: APIRouter, completion:@escaping(Result<T>)->Void) {
        do {
            let task = try session.dataTask(with: route.request()) { (data, response, error) in
                
                DispatchQueue.main.async {
                    // Step 1 : error case
                    if let error = error {
                        completion(Result<T>.failure(error: error))
                        return
                    }
                            
                    // Step 2 : status Code Analyse
                    if let httpResponse = response as? HTTPURLResponse {
                        print("Status Code: \(httpResponse.statusCode)")
                        
                        if (200...299).contains(httpResponse.statusCode) {
                            guard let data = data else {
                                completion(Result<T>.failure(error: APIError.defaultError))
                                return
                            }
                                       
                            do { // Step 2.1 : Success
                                print("data",data)
                                let result = try JSONDecoder().decode(T.self, from: data)
                                completion(Result.success(data: result))
                            } catch let error {
                                print("erorrnr", error.localizedDescription)
                                completion(Result.failure(error: error))
                            }
                                
                        } else { // Step 2.2 : error code
                            completion(Result<T>.failure(error: APIError.getError(errorCode: httpResponse.statusCode)))
                            return
                        }
                    }
                }
            }
            task.resume()
                
            } catch let error {
                completion(Result<T>.failure(error: error))
            }
        }
    
}
