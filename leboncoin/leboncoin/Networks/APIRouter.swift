//
//  APIRouter.swift
//  leboncoin
//
//  Created by Mac2 on 21/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation

enum APIRouter {
    
    case getItemList
    case getCategoryList
    
    // MARK: - HTTPMethod
    private enum HTTPMethod {
        case get
        case post
        case put
        case delete
           
        var value: String {
            switch self {
               case .get: return "GET"
               case .post: return "POST"
               case .put: return "PUT"
               case .delete: return "DELETE"
            }
        }
    }
    
    private var method: HTTPMethod {
        switch self {
        case .getItemList: return .get
        case .getCategoryList: return .get
        }
    }

    // MARK: - Path
    private var path: String {
        switch self {
        case .getItemList:
            return EnvironmentConfiguration.getKeyvalue(key: "ItemListPath")
        case .getCategoryList:
            return EnvironmentConfiguration.getKeyvalue(key: "CategoryListPath")
        }
    }
    
    // MARK: - Request
    func request() throws -> URLRequest {
        
        let baseURLString = EnvironmentConfiguration.getKeyvalue(key: "BaseUrl")
        let urlString = baseURLString + path
        
        guard let url = URL(string: urlString) else {
            throw APIError.parseUrlError
        }
        
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        request.httpMethod = method.value
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        switch self {
        case .getItemList, .getCategoryList:
            return request
        }
    }
    
}



