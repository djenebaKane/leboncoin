//
//  APIError.swift
//  leboncoin
//
//  Created by Mac2 on 21/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation


enum APIError : Error {

    case parseUrlError

    case unauthorizedError // code 401
    
    case forbiddenError // code 403
    
    case notFoundError // code 404
    
    case serverError // code 500 && 503
    
    case defaultError
    
    static func getError(errorCode: Int)-> APIError{
        
        var error : APIError
        
        switch errorCode {
            
        case 401:
            error = .unauthorizedError
            
        case 403:
            error = .forbiddenError
            
        case 404:
            error = .notFoundError
            
        case 500, 503:
            error = .serverError
            
        default:
            error = .defaultError
        }
        return error
    }
}
    
extension APIError: LocalizedError {
    public var errorDescription: String? {
        var str: String?
        
        switch self {
        case .parseUrlError:
            str = "Url parse error"
        case .forbiddenError:
            str = "Forbidden"
            
        case .unauthorizedError:
            str = "Not authorized"
            
        case .notFoundError:
            str = "Not Found"
        
        case .serverError:
            str = "Oops, Serveur Error"
        default:
            str =  self.localizedDescription
        }
        return str
    }
}
