//
//  EnvironnementConfiguration.swift
//  leboncoin
//
//  Created by Mac2 on 21/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation

class EnvironmentConfiguration: NSObject {

    class func getKeyvalue(key : String) -> String
    {
        return Bundle.main.infoDictionary?[key] as? String ?? ""
    }
    
    class func getKeyvalueBool(key : String) -> Bool
    {
        return Bundle.main.infoDictionary?[key] as? Bool ?? false
    }
    class func getList(key:String) -> [String]{
        return Bundle.main.infoDictionary?[key] as? [String] ?? [""]
    }

}
