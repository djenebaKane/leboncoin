//
//  ItemCell.swift
//  leboncoin
//
//  Created by Mac2 on 22/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import UIKit

class ItemCell : UITableViewCell {
    

    // - UITableViewCell Outlets
    private let progressView : UIActivityIndicatorView = {
        let prgView = UIActivityIndicatorView(style: .gray)
        prgView.hidesWhenStopped = true
        prgView.color = UIColor.accentColor
        return prgView
    }()
    
    private let itemTitleLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textAccentColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let itemCategoryLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textColor
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textAlignment = .left
        return lbl
    }()
    
    private let itemPriceLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .textAccentColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        return lbl
    }()
    private let itemUrgentLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .accentColor
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .right
    
        return lbl
    }()
    
    private let itemImage : UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true

    return imgView
    }()
    
    
    static let cellId = "ItemCell"
    
    var item : Item? {
        didSet {
            if let item = item {
                // - TODo : begin loding
                progressView.startAnimating()
                self.itemImage.image = nil
                ImageUtils.downloadImageAsynchronous(urlString: item.images_url.small){ image  in
                    self.progressView.stopAnimating()
                    if let img = image {
                        self.itemImage.image = ImageUtils.resizingImage(image: img, scaleSize: self.itemImage.bounds.size)
                    }
                }
                
                self.itemTitleLabel.text = item.title
                self.itemPriceLabel.text = "\(item.price)" + " €"
                self.itemUrgentLabel.text = "Urgent"
                self.itemUrgentLabel.isHidden = !item.is_urgent
                self.itemCategoryLabel.text = item.getCategoryName(catList: ItemListViewController.categoryList)
                
            }
        }
    }
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(itemTitleLabel)
        addSubview(itemCategoryLabel)
        addSubview(itemPriceLabel)
        addSubview(itemUrgentLabel)
        addSubview(itemImage)
        addSubview(progressView)
        
        itemImage.anchor(top: nil, left: leftAnchor, bottom: nil, right: itemTitleLabel.leftAnchor, paddingTop: 10, paddingLeft: 5, paddingBottom: 10, paddingRight: 10, width: 80, height: 80, enableInsets: true)
        
        progressView.anchor(top: itemImage.topAnchor, left: itemImage.leftAnchor, bottom: itemImage.bottomAnchor, right: itemImage.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        itemImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        itemTitleLabel.anchor(top: topAnchor, left: nil, bottom: itemCategoryLabel.topAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)
    
        itemCategoryLabel.anchor(top: nil, left: itemTitleLabel.leftAnchor, bottom: itemPriceLabel.topAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 10, paddingRight: 10)
        
        itemPriceLabel.anchor(top: nil, left: itemTitleLabel.leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 10, paddingRight: 10)
        
        itemUrgentLabel.anchor(top: nil, left: itemPriceLabel.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
