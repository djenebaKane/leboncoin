//
//  DownloadImage.swift
//  leboncoin
//
//  Created by Mac2 on 22/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import UIKit
public class ImageUtils {
    
    /**
     Download Image from URL  String asynchroniously.
     - parameter : (urlString: String?)
     - returns: (@escaping ((UIImage?) -> Void))
     */
    static func downloadImageAsynchronous(urlString: String?, completionHandler: @escaping ((UIImage?) -> Void)){
        if let urlString = urlString, let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else {
                        DispatchQueue.main.async() {
                            completionHandler(nil)
                        }
                    return
                }
                DispatchQueue.main.async() {
                    completionHandler(image)
                }
                }.resume()
            
        } else {
            completionHandler(nil)
        }
    }
    
    /**
     Rezine image to match a specific size.
     - parameter : (image: UIImage, scaleSize: CGSize
     - returns: (UIImage?)
     */
    static func resizingImage(image: UIImage, scaleSize: CGSize) -> UIImage? {
        
        let factor = image.size.height / scaleSize.height
        
        let newHeight = image.size.height / factor
        
        let newWidth = image.size.width / factor
        
        let size = CGSize(width: newWidth, height: newHeight)
        
        //avoid redundant drawing
        if __CGSizeEqualToSize(image.size, size) {
            return image
        }
        
        //create drawing context
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        //draw
        image.draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        
        //capture resultant image
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

}
