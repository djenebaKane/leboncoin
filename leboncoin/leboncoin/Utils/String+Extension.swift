//
//  String+Extension.swift
//  leboncoin
//
//  Created by Mac2 on 22/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation

extension String {
    /**
     Return Date in MM/dd/yyyy format.
     - parameter : ()
     - returns: (String)
     */
    func dateUTCToLocal() -> String {
        if let dateFrom = self.components(separatedBy: ".").first {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss+0000"
         //   dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            
            let dateFormatterSet = DateFormatter()
            dateFormatterSet.timeZone = TimeZone.current
            dateFormatterSet.dateFormat = "MM/dd/yyyy"
            
            if let date = dateFormatterGet.date(from: dateFrom) {
                return dateFormatterSet.string(from: date)
            }
        }
        return self
    }
    
    /**
     Transform String to Date.
     - parameter : ()
     - returns: (Date?)
     */
    func toDate() -> Date? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-ddTHH:mm:ss+0000"
        return dateFormatterGet.date(from: self)
    }
}
