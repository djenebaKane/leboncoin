//
//  UIViewController+Extension.swift
//  leboncoin
//
//  Created by Mac2 on 24/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import UIKit

extension UIViewController {
    

    func displayAlert(title: String? = nil, msg: String? = nil, style : UIAlertController.Style = .alert){
        let alert = UIAlertController(title:title, message:
            msg, preferredStyle: style)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
}
