//
//  UIColor+Extension.swift
//  leboncoin
//
//  Created by Mac2 on 24/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    public static let accentColor =  UIColor(red: 245.0/255.0, green: 107.0/255.0, blue: 42.0/255.0, alpha: 1.0)

    public static let accentLightColor =  UIColor(red: 255.0/255.0, green: 194.0/255.0, blue: 164.0/255.0, alpha: 1.0)

    public static let primaryColor = UIColor(red: 8.0/255, green: 60.0/255.0, blue: 110/255.0, alpha: 1.0)

    public static let primaryDarkColor = UIColor(red: 4.0/255, green: 45.0/255.0, blue: 84.0/255.0, alpha: 1.0)

    public static let textColor = UIColor(red: 66.0/255.0, green: 60.0/255.0, blue: 58.0/255.0, alpha: 1.0)
    
    public static let textAccentColor = UIColor(red: 48.0/255.0, green: 43.0/255.0, blue: 41.0/255.0, alpha: 1.0)
    
    
    
}
