//
//  MockMode.swift
//  leboncoinTests
//
//  Created by Mac2 on 24/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import XCTest
@testable import leboncoin

extension leboncoin.Item {
    static func with(id : Int = 0,
                     category_id : Int = 0,
                     title : String = "Title",
                     description : String = "description",
                     price : Float = 0.0,
                     images_url : ImageSize,
                     creation_date : String = "",
                     is_urgent : Bool = true,
                     siret : String? = nil) -> Item {
        
        return leboncoin.Item(id: id, category_id: category_id, title: title, description: description, price: price, images_url: images_url, creation_date: creation_date, is_urgent : is_urgent, siret: siret)
            
    }
    
}

extension leboncoin.Category {
    static func with(id : Int = 0,
                     name : String = "Mode") -> leboncoin.Category {
        
        return leboncoin.Category(id: id, name: name)
    }
}



 extension leboncoin.Item {
    init?(json: Dictionary<String,Any>){
        guard let id = json["id"] as? Int,
              let category_id = json["category_id"] as? Int,
              let title = json["title"] as? String,
              let description = json["description"] as? String,
              let price = json["price"] as? Float,
              let images_url = json["images_url"] as? [String: String],
              let small = images_url["small"],
              let thumb = images_url["thumb"],
              let creation_date = json["creation_date"] as? String,
              let is_urgent = json["is_urgent"] as? Bool,
              let siret = json["siret"] as? String?
              else {
            return nil
        }
        
        self.init(id: id, category_id: category_id, title: title, description: description, price: price, images_url: ImageSize(small: small, thumb: thumb), creation_date: creation_date, is_urgent: is_urgent, siret: siret)
    }
}

 extension leboncoin.Category {
    init?(json: Dictionary<String,Any>){
        guard let id = json["id"] as? Int,
              let name = json["name"] as? String else {
            return nil
        }
        
        self.init(id: id, name: name)
    }
}
