//
//  ModelTests.swift
//  leboncoinTests
//
//  Created by Mac2 on 24/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import XCTest
@testable import leboncoin

class ModelTests: XCTestCase {

    
    func testSucessFullInitItem() {
        let testSuccessFulJson : Dictionary<String,Any> = ["id" : 0,
                                         "category_id" :0,
                                         "title" : "Title",
                                         "description" : "description",
                                         "price" : Float(0.0),
                                         "images_url" : ["small": "smallUrl", "thumb" : "thumbUrl"],
                                         "creation_date" : "2019-11-06T11:18:09+0000",
                                         "is_urgent" :  true,
                                         "siret" :  "siret"]
        
        XCTAssertNotNil(Item(json: testSuccessFulJson))
    }
    
   func testErrorFullInitItem() {
       let testErrorFulJson : Dictionary<String,Any> = ["id" : "0",
                                        "category_ids" :0,
                                        "title" : "Title",
                                        "description" : "description"
                                        ]
       
       XCTAssertNil(Item(json: testErrorFulJson))
   }
   
    
    
    func testSucessFullInitCategory() {
        let testErrorFulJson : Dictionary<String,Any> = ["id" : 0,
                                         "name" :"Mode"]
        
        XCTAssertNotNil(leboncoin.Category(json: testErrorFulJson))
    }
    
    func testErrorFullInitCategory() {
        let testSuccessFulJson : Dictionary<String,Any> = ["id" : "0",
                                         "name" :"Mode"]
        
        XCTAssertNil(leboncoin.Category(json: testSuccessFulJson))
    }
    

}
