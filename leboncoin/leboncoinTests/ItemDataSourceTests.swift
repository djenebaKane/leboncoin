//
//  ItemDataSourceTests.swift
//  leboncoinTests
//
//  Created by Mac2 on 24/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import XCTest
@testable import leboncoin


class ItemDataSourceTests : XCTest {
    
    var dataSource : ItemDataSource?
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        dataSource = ItemDataSource()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        dataSource = nil
        super.tearDown()

    }

    func testValueInDataSource() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        
        let item1 = Item(id: 1582151024, category_id: 4, title: "Grandes bouteilles de vin vides", description: "2 grandes bouteilles de vin...", price: 6.0, images_url: ImageSize(small: "smallUrl", thumb: "thumbUrl"), creation_date: "2019-11-06T11:18:02+0000", is_urgent: true, siret: nil)
        
        let item2 = Item(id: 1582151025, category_id: 2, title: "Grandes bouteilles de vin vides", description: "2 grandes bouteilles de vin...", price: 6.0, images_url: ImageSize(small: "smallUrl", thumb: "thumbUrl"), creation_date: "2019-11-06T11:18:02+0000", is_urgent: true, siret: nil)
            
        let item3 = Item(id: 1582151026, category_id: 3, title: "Grandes bouteilles de vin vides", description: "2 grandes bouteilles de vin...", price: 6.0, images_url: ImageSize(small: "smallUrl", thumb: "thumbUrl"), creation_date: "2019-11-06T11:18:02+0000", is_urgent: true, siret: nil)
        
        dataSource?.data.value = [item1, item2, item3]
        let tableView = UITableView()
        
        tableView.dataSource = dataSource
        
        // expect one section
        XCTAssertEqual(dataSource?.numberOfSections(in: tableView), 1, "Expect one section in tableView")
        
        // expect 3 cells
        XCTAssertEqual(dataSource?.tableView(tableView, numberOfRowsInSection: 0), 3, "Expect three cells in tableView")
    }
    
}
