//
//  ModelViewModelTests.swift
//  leboncoinTests
//
//  Created by Mac2 on 24/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import XCTest
@testable import leboncoin

class ModelViewModelTests: XCTestCase {

    var mockApiClient = MockAPIClient()
    let itemDataSource = ItemDataSource()
    
    
    // - Item ModelView Test
    func testSuccessItemList() {
        
        mockApiClient.itemResult = .success(data: [Item.with(id: 0, category_id: 0, title: "title", description: "description", price: Float(2), images_url: ImageSize(small: "small", thumb: "thumb"), creation_date: "2019-11-06T11:18:02+0000", is_urgent: true, siret: "000 111 222")])

        let viewModel = ItemViewModel(dataSource: itemDataSource)
        
        
        viewModel.fetchItems()
        
        switch mockApiClient.itemResult {
        case .success(data: let items):
            viewModel.dataSource?.data.value = items

        case .failure(error:  _):
            XCTFail()
            break
        case .none:
            XCTFail()
            break
        }
        
    }
    
    
    func testErrorItemList() {
        
        mockApiClient.itemResult = .failure(error: APIError.parseUrlError)

        let viewModel = ItemViewModel(dataSource: itemDataSource)
        
        
        viewModel.fetchItems()
        
        switch mockApiClient.itemResult {
        case .success(data: let items):
            viewModel.dataSource?.data.value = items
            XCTFail()
            break
        case .failure(error:  _):
            break
        case .none:
            XCTFail()
            break
        }
        
    }
    
    // - Category ModelView Test

    func testSuccessCategoryList() {
        mockApiClient.categoryResult = .success(data: [leboncoin.Category.init(id: 0, name: "Loisirs")])
        switch mockApiClient.categoryResult {
        case .success(data: _):
            break

        case .failure(error:  _):
            XCTFail()
            break
        case .none:
            XCTFail()
            break
        }
        
    }
    
    
    func testErrorCategoryList() {
        mockApiClient.categoryResult = .failure(error: APIError.parseUrlError)
        switch mockApiClient.categoryResult {
        case .success(data: _):
            XCTFail()
            break
        case .failure(error:  _):
            break
        case .none:
            XCTFail()
            break
        }
        
    }
}
