//
//  MockAPIClient.swift
//  leboncoinTests
//
//  Created by Mac2 on 24/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation

import Foundation
import XCTest
@testable import leboncoin

class MockAPIClient : APIClient {
    
    var itemResult : Client.Result<[Item]>?
    var categoryResult : Client.Result<[leboncoin.Category]>?
    override func getItemList(completion: @escaping (Client.Result<[Item]>) -> Void) {
        completion(itemResult ?? Client.Result.success(data: [Item].init()))
    }
    
    override func getCategoryList(completion: @escaping (Client.Result<[leboncoin.Category]>) -> Void) {
        completion(categoryResult ?? Client.Result.success(data: [leboncoin.Category].init()))
    }
}
