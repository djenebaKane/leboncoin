//
//  CategoryDataSourceTests.swift
//  leboncoinTests
//
//  Created by Mac2 on 24/03/2020.
//  Copyright © 2020 Djeneba KANE. All rights reserved.
//

import Foundation
import XCTest
@testable import leboncoin


class CategoryDataSourceTests : XCTest {
    
    var dataSource : CategoryDataSource?
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        dataSource = CategoryDataSource()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        dataSource = nil
        super.tearDown()

    }

    func testValueInDataSource() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        
        let category1 = leboncoin.Category(id: 1, name: "Loisirs")
        let category2 = leboncoin.Category(id: 2, name: "Loisirs")
        
        dataSource?.data.value = [category1, category2]
        let tableView = UITableView()
        
        tableView.dataSource = dataSource
        
        // expect one section
        XCTAssertEqual(dataSource?.numberOfSections(in: tableView), 1, "Expect one section in tableView")
        
        // expect 3 cells
        XCTAssertEqual(dataSource?.tableView(tableView, numberOfRowsInSection: 0), 2, "Expect three cells in tableView")
    }
    
}
